# Pickin' Sticks LXXIV v2

A pure-Java version of Pickin' Sticks LXXIV written for Android.
Doing this to get some idea of how annoying (or how possible) it is
to write a game using just Java in Android Studio, so I can decide
whether I want to do so for future mobile projects.

Additionally to try out different things on Android, such as
different control schemes I want to try out, before implementing
it in an actual game.

## Yob framework

My reusable code is in the yob repository here:
https://bitbucket.org/moosaderllc/yob-framework/src

This should be cloned into the com/moosader/ directory,
and the folder should be named "yob".
