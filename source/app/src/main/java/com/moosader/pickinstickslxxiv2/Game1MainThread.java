package com.moosader.pickinstickslxxiv2;

import android.graphics.Canvas;
import android.view.SurfaceHolder;

public class Game1MainThread extends Thread {
    private SurfaceHolder surfaceHolder;
    private Game1View gameView;
    private boolean running;
    public static Canvas canvas;

    public Game1MainThread(SurfaceHolder surfaceHolder, Game1View gameView)
    {
        super();
        this.surfaceHolder = surfaceHolder;
        this.gameView = gameView;
    }

    @Override
    public void run()
    {
        while(running)
        {
            canvas = null;

            try
            {
                canvas = this.surfaceHolder.lockCanvas();
                synchronized(surfaceHolder) {
                    this.gameView.update();
                    this.gameView.draw(canvas);
                }
            }
            catch( Exception e )
            {
            }
            finally
            {
                if ( canvas != null )
                {
                    try
                    {
                        surfaceHolder.unlockCanvasAndPost(canvas);
                    }
                    catch( Exception e )
                    {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    public void setRunning(boolean isRunning)
    {
        running = isRunning;
    }
}
