package com.moosader.pickinstickslxxiv2;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class Game1Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(new Game1View(this));
    }
}
