package com.moosader.pickinstickslxxiv2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Fullscreen time
        //this.getWindow().setFlags( WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN );

        // Set no title
        //this.requestWindowFeature( Window.FEATURE_NO_TITLE );

        gotoGame1();
    }

    public void gotoGame1(View view)
    {
        Intent intent = new Intent( this, Game1Activity.class);
        startActivity(intent);
    }

    public void gotoGame1()
    {
        Intent intent = new Intent( this, Game1Activity.class);
        startActivity(intent);
    }
}
