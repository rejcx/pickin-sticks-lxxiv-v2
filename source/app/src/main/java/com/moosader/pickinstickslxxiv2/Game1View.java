package com.moosader.pickinstickslxxiv2;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Rect;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceView;
import android.view.SurfaceHolder;

import com.moosader.yob.Objects.BaseObject;
import com.moosader.yob.Objects.Properties.Drawable;
import com.moosader.yob.Objects.TapCharacter;
import com.moosader.yob.Utilities.GraphicsUtility;

import java.util.ArrayList;

public class Game1View extends SurfaceView implements SurfaceHolder.Callback {
    private Game1MainThread thread;
    private float dpi;

    // https://www.androidauthority.com/android-game-java-785331/
    // https://o7planning.org/en/10521/android-2d-game-tutorial-for-beginners#a1815842

    public Game1View(Context context) {
        super(context);
        getHolder().addCallback(this);
        thread = new Game1MainThread(getHolder(), this);
        setFocusable(true);
        GraphicsUtility.setup( context,
                Resources.getSystem().getDisplayMetrics().widthPixels,
                Resources.getSystem().getDisplayMetrics().heightPixels,
                720, 1280
                );
        setup();
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        thread.setRunning(true);
        thread.start();
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        // This doesn't make sense logically
        boolean retry = true;
        while ( retry )
        {
            try {
                thread.setRunning(false);
                thread.join();
            }
            catch( InterruptedException e )
            {
                e.printStackTrace();
            }
            retry = false;
        }
    }

    @Override
    public boolean onTouchEvent( MotionEvent event )
    {
        return handleTouch( event );
    }

    /* Game Content */
    BaseObject stick = new BaseObject();
    TapCharacter player = new TapCharacter();
    ArrayList<BaseObject> tiles = new ArrayList<BaseObject>();
    Drawable tileDrawable = new Drawable();
    Bitmap image;

    public void setup() {
        stick.getDrawable().setImage(BitmapFactory.decodeResource(getResources(), R.drawable.stick));
        stick.getDrawable().setPosition( (int)GraphicsUtility.pxToDp( 80 ), 0, (int)GraphicsUtility.pxToDp( 40 ), (int)GraphicsUtility.pxToDp( 40 ) );
        stick.getDrawable().setFrame( 0, 0,  (int)GraphicsUtility.pxToDp( 40 ), (int)GraphicsUtility.pxToDp( 40 ) );
        stick.getDrawable().debug();

        player.setup( "player" );
        player.getAnimatable().setup( BitmapFactory.decodeResource(getResources(), R.drawable.pinkcat), 1, 2, 0.1 );
        player.getMovable().setup( 200, 200, 10 );

        /*
        int tileWidthPx = 40;
        int tileHeightPx = 40;
        float tileWidthDp = GraphicsUtility.pxToDp( tileWidthPx );
        float tileHeightDp = GraphicsUtility.pxToDp( tileWidthPx );

        int screenWidthPx = GraphicsUtility.dpToPx( GraphicsUtility.getScreenWidth() );
        int screenHeightPx = GraphicsUtility.dpToPx( GraphicsUtility.getScreenHeight() );

        tileDrawable.setImage( BitmapFactory.decodeResource(getResources(), R.drawable.tileset), 0, 0, (int)tileWidthDp, (int)tileHeightDp );

        Rect source = new Rect( 0, 0, 40, 40 );
        Rect dest = new Rect( 0, 0, 80, 80 );
        */

        // Create background
        /*
        for ( int y = 0; y < screenHeightPx / tileHeightPx; y++ ) {
            for ( int x = 0; x < screenWidthPx / tileWidthPx; x++ ) {
                BaseObject tile = new BaseObject();
                // TODO: Research whether this references the single image, or makes a copy.
                // This is Java so I think it makes references.
                tile.getDrawable().setImage( tileDrawable.getImage() );
                float xDp = GraphicsUtility.pxToDp( x * tileWidthPx );
                float yDp = GraphicsUtility.pxToDp( y * tileHeightPx );
                tile.getDrawable().setPosition( (int)xDp, (int)yDp );
                tiles.add( tile );
            }
        }
        */

        //Log.i( "Amount of tiles", Integer.toString( tiles.size() ) );
    }

    public void update() {
        player.update();
    }

    @Override
    public void draw(Canvas canvas) {
        super.draw(canvas);
        if ( canvas != null ) {
            canvas.drawColor(Color.BLUE);

            /*
            for ( int i = 0; i < tiles.size(); i++ ) {
                tiles.get( i ).draw( canvas );
            }
*/

            //canvas.drawBitmap( tileDrawable.getImage(), 0, 0, null );

            player.draw(canvas);
            stick.draw(canvas);
        }
    }

    public boolean handleTouch(MotionEvent event)
    {
        if ( event.getAction() == MotionEvent.ACTION_DOWN ||
            event.getAction() == MotionEvent.ACTION_MOVE )
        {
            int touchX = (int)event.getX();
            int touchY = (int)event.getY();

            player.getMovable().setTarget( touchX, touchY );

            return true;
        }

        return false;
    }
}
